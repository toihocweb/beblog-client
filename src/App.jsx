import React from 'react';
// import Login from './components/Login';
import Register from './components/Register';

const App = () => {
  return (
    <div className='App'>
      {/* <Login /> */}
      <Register />
    </div>
  );
};

export default App;
