import React from 'react';
import { Row, Col, Form, Button, Input } from 'antd';

const ColResponsive = {
  xs: 10,
  sm: 8,
  md: 14,
  lg: 8,
};
const Login = () => {
  const OnFinish = (vals) => {
    console.log(vals);
  };
  return (
    <div className='Login' style={{ paddingTop: 100, background: 'none' }}>
      <Row justify='center'>
        <Col
          {...ColResponsive}
          style={{ padding: 15, boxShadow: '2px 3px 5px 4px #969393' }}
        >
          <h1 style={{ textAlign: 'center', marginBottom: 30 }}>
            BEBLOG-CLIENT
          </h1>
          <Form onFinish={OnFinish}>
            <Form.Item
              name='username'
              label={<strong>USERNAME </strong>}
              rules={[
                {
                  required: true,
                  message: 'Please input your username',
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              name='password'
              label={<strong>PASSWORD </strong>}
              rules={[
                {
                  required: true,
                  message: 'Please input your password',
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item>
              <Row>
                <Col span={12}>
                  <Button type='link' htmlType='button'>
                    Sign Up
                  </Button>
                </Col>
                <Col span={12}>
                  <Button type='link' htmlType='button'>
                    Forgot your password ?
                  </Button>
                </Col>
              </Row>
            </Form.Item>
            <Form.Item>
              <Button type='primary' htmlType='submit'>
                LOGIN
              </Button>
            </Form.Item>
          </Form>
        </Col>
      </Row>
    </div>
  );
};

export default Login;
