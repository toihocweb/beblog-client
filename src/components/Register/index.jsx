import React from 'react';
import { Row, Col, Button, Form, Input } from 'antd';
import API from '../../api/Api';
import axios from 'axios';
const ResponsiveCol = {
  xs: 10,
  sm: 10,
  md: 12,
  lg: 12,
};
const formLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 24,
    },
    md: {
      span: 9,
    },
    lg: {
      span: 8,
    },
  },
  wrapperCol: {
    lg: {
      span: 10,
    },
  },
};

const POST = async (reg_values) => {
  await axios({
    method: 'post',
    baseURL: API,
    url: '/v1/auth/register',
    data: { ...reg_values },
    timeout: 1500,
  });
};

const Register = () => {
  const onFinish = async (vals) => {
    try {
      await POST({ ...vals });
    } catch (error) {
      alert(error.message);
    }
  };
  return (
    <div className='Register' style={{ paddingTop: 125 }}>
      <div>
        <h1 style={{ textAlign: 'center' }}>BEBLOG-CLIENT</h1>
        <h1 style={{ textAlign: 'center' }}>BEBLOG-REGISTER</h1>
      </div>
      <Row justify='center' style={{ paddingRight: 15 }}>
        <Col {...ResponsiveCol}>
          <Form {...formLayout} onFinish={async (vals) => await onFinish(vals)}>
            {/* <Form.Item
              name='firstname'
              label={<strong>First Name</strong>}
              rules={[
                {
                  required: true,
                  message: 'Please input your username',
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              name='lastname'
              label={<strong>Last Name</strong>}
              rules={[
                {
                  required: true,
                  message: 'Please input your last name',
                },
              ]}
            >
              <Input />
            </Form.Item> */}

            <Form.Item
              name='name'
              label={<strong>UserName</strong>}
              rules={[
                {
                  required: true,
                  message: 'Please input your username',
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              name='email'
              label={<strong>Email </strong>}
              rules={[
                {
                  type: 'email',
                  message: 'Invalid Email',
                },
                {
                  required: true,
                  message: 'Please input your Email',
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              name='password'
              label={<strong>Password </strong>}
              rules={[
                {
                  required: true,
                  message: 'Please input your password',
                },
              ]}
            >
              <Input.Password />
            </Form.Item>
            <Form.Item
              name='password_confirm'
              label={<strong>Confirm Password</strong>}
              dependencies={['password']}
              rules={[
                {
                  required: true,
                  message: 'Please input your password again',
                },
                ({ getFieldValue }) => ({
                  validator(rule, value) {
                    if (!value || getFieldValue('password') === value) {
                      return Promise.resolve();
                    }
                    return Promise.reject('The two passwords do not match');
                  },
                }),
              ]}
            >
              <Input.Password />
            </Form.Item>
            <Form.Item
              wrapperCol={{
                xs: {
                  offset: 0,
                },
                md: {
                  offset: 9,
                },
                lg: {
                  offset: 8,
                },
              }}
            >
              <Button htmlType='submit' type='primary'>
                Register
              </Button>
            </Form.Item>
          </Form>
        </Col>
      </Row>
    </div>
  );
};

export default Register;
